/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.eir.ecom.db.dao;

import com.eir.ecom.db.model.Category;
import java.util.List;
import javax.persistence.NoResultException;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Andri D. Septian
 */
@Repository(value = "CategoryDao")
@Transactional
public class CategoryDao extends Dao {

    Query Query = new Query();

    public Category saveorupdate(Category dat) {
        try {
            if (dat.getCategoryId() == null) {
                em.persist(dat);
            } else {
                em.merge(dat);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
        return dat;
    }

    public List<Category> findAll() {
        try {
            return (List<Category>) em.createQuery("SELECT a FROM Category a")
                    .getResultList();
        } catch (NoResultException nre) {
            return null;
        }
    }

    public Category findById(Long id) {
        try {
            return (Category) em.createQuery("SELECT a FROM Category a WHERE a.categoryId = :id")
                    .setParameter("id", id)
                    .setMaxResults(1)
                    .getSingleResult();
        } catch (NoResultException nre) {
            return null;
        }
    }

    public Category findByCategoryName(String name) {
        try {
            return (Category) em.createQuery("SELECT a FROM Category a WHERE a.categoryName = :name")
                    .setParameter("name", name)
                    .setMaxResults(1)
                    .getSingleResult();
        } catch (NoResultException nre) {
            return null;
        }
    }

    public class Query {

        Native Native = new Native();

        public Category single(String query) {
            try {
                return (Category) em.createQuery(query, Category.class)
                        .setMaxResults(1)
                        .getSingleResult();
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
        }

        public List<Category> list(String query) {
            try {
                return (List<Category>) em.createQuery(query, Category.class)
                        .getResultList();
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
        }

        public class Native {

            public Category single(String query) {
                try {
                    return (Category) em.createNativeQuery(query, Category.class)
                            .setMaxResults(1)
                            .getSingleResult();
                } catch (Exception e) {
                    e.printStackTrace();
                    return null;
                }
            }

            public List<Category> list(String query) {
                try {
                    return (List<Category>) em.createNativeQuery(query, Category.class)
                            .getResultList();
                } catch (Exception e) {
                    e.printStackTrace();
                    return null;
                }
            }
        }
    }
}
