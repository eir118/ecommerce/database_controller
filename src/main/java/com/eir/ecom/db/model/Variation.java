/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.eir.ecom.db.model;

import com.eir.ecom.db.enums.AdditionalData;
import com.eir.ecom.db.enums.Status;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PreUpdate;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import org.hibernate.annotations.Type;
import org.json.JSONObject;

/**
 *
 * @author Andri D. Septian
 */

/*
variation_id
product_id
variation_name
stock
price
status
description
info
additional_data
 */
@Entity
@Table(name = "product_variation")
public class Variation implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY, generator = "variation_id_seq")
    @SequenceGenerator(name = "variation_id_seq", sequenceName = "variation_id_seq")
    @Column(name = "variationId", nullable = false)
    private Long variationId;

    @Column(name = "cr_time", nullable = false)
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date crTime = new Date();

    @Column(name = "mod_time")
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date modTime;

    @ManyToOne
    @JoinColumn(name = "product_id", nullable = false)
    private Product productId;

    @Column(name = "variation_name")
    private String variationName;

    @Column(name = "stock", nullable = false)
    private Integer stock = 0;

    @Column(name = "price", nullable = false)
    private Double price = 0.0;

    @Column(name = "status", nullable = false)
    private Status.Data status = Status.Data.DRAFT;

    @Column(name = "description")
    private String description;

    @Column(name = "info")
    private String info;

    @PreUpdate
    protected void onUpdate() {
        setModTime(new Date());
    }

    public Long getVariationId() {
        return variationId;
    }

    public void setVariationId(Long variationId) {
        this.variationId = variationId;
    }

    public Date getCrTime() {
        return crTime;
    }

    public void setCrTime(Date crTime) {
        this.crTime = crTime;
    }

    public Date getModTime() {
        return modTime;
    }

    public void setModTime(Date modTime) {
        this.modTime = modTime;
    }

    public Product getProductId() {
        return productId;
    }

    public void setProductId(Product productId) {
        this.productId = productId;
    }

    public String getVariationName() {
        return variationName;
    }

    public void setVariationName(String variationName) {
        this.variationName = variationName;
    }

    public Integer getStock() {
        return stock;
    }

    public void setStock(Integer stock) {
        this.stock = stock;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Status.Data getStatus() {
        return status;
    }

    public void setStatus(Status.Data status) {
        this.status = status;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    @Column(name = "additionalData")
    @Type(type = "text")
    private String additionalData;

    public JSONObject getAdditionalData() {
        return new JSONObject(additionalData);
    }

    public void setAdditionalData(JSONObject additionalData) {
        this.additionalData = additionalData.toString();
    }

}
