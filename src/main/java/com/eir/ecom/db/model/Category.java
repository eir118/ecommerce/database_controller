/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.eir.ecom.db.model;

import com.eir.ecom.db.enums.AdditionalData;
import com.eir.ecom.db.enums.Status;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.PreUpdate;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import org.hibernate.annotations.Type;
import org.json.JSONObject;

/**
 *
 * @author Andri D. Septian
 */
@Entity
@Table(name = "product_category")
public class Category implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY, generator = "category_id_seq")
    @SequenceGenerator(name = "category_id_seq", sequenceName = "category_id_seq")
    @Column(name = "category_id", nullable = false)
    private Long categoryId;

    @Column(name = "cr_time", nullable = false)
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date crTime = new Date();

    @Column(name = "mod_time")
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date modTime;

    @Column(name = "category_name", nullable = false)
    private String categoryName;

    @Column(name = "description")
    @Type(type = "text")
    private String description;

    @Column(name = "status", nullable = false)
    private Status.Data status = Status.Data.DRAFT;

    @Column(name = "additionalData")
    @Type(type = "text")
    private String additionalData;

    public JSONObject getAdditionalData() {
        return new JSONObject(additionalData);
    }

    public void setAdditionalData(JSONObject additionalData) {
        this.additionalData = additionalData.toString();
    }


    @PreUpdate
    protected void onUpdate() {
        setModTime(new Date());
    }

    public Long getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Long categoryId) {
        this.categoryId = categoryId;
    }

    public Date getCrTime() {
        return crTime;
    }

    public void setCrTime(Date crTime) {
        this.crTime = crTime;
    }

    public Date getModTime() {
        return modTime;
    }

    public void setModTime(Date modTime) {
        this.modTime = modTime;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Status.Data getStatus() {
        return status;
    }

    public void setStatus(Status.Data status) {
        this.status = status;
    }

}
