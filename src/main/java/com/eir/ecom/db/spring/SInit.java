/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.eir.ecom.db.spring;

import com.eir.ecom.db.dao.CategoryDao;
import com.eir.ecom.db.dao.ConfigurationDao;
import com.eir.ecom.db.dao.Dao;
import com.eir.ecom.db.dao.ProductDao;
import com.eir.ecom.db.dao.ResponseMessageDao;
import com.eir.ecom.db.dao.SessionsDao;
import com.eir.ecom.db.dao.UserAccountDao;
import com.eir.ecom.db.dao.VariationDao;
import java.util.Calendar;
import javax.naming.ConfigurationException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.FileSystemXmlApplicationContext;

/**
 *
 * @author Andri D. Septian
 */
public class SInit {

    public static Dao dao;

    public static UserAccountDao userAccountDao;
    public static SessionsDao sessionsDao;
    public static ResponseMessageDao responseMessageDao;
    public static ProductDao productDao;
    public static CategoryDao categoryDao;
    public static VariationDao variationDao;
    public static ConfigurationDao configurationDao;

    public static Dao getDao() {
        return dao;
    }

    public static void setDao(Dao dao) {
        SInit.dao = dao;
    }

    public static UserAccountDao getUserAccountDao() {
        return userAccountDao;
    }

    public static void setUserAccountDao(UserAccountDao userAccountDao) {
        SInit.userAccountDao = userAccountDao;
    }

    public static SessionsDao getSessionsDao() {
        return sessionsDao;
    }

    public static void setSessionsDao(SessionsDao sessionsDao) {
        SInit.sessionsDao = sessionsDao;
    }

    public static ResponseMessageDao getResponseMessageDao() {
        return responseMessageDao;
    }

    public static void setResponseMessageDao(ResponseMessageDao responseMessageDao) {
        SInit.responseMessageDao = responseMessageDao;
    }

    public static ProductDao getProductDao() {
        return productDao;
    }

    public static void setProductDao(ProductDao productDao) {
        SInit.productDao = productDao;
    }

    public static CategoryDao getCategoryDao() {
        return categoryDao;
    }

    public static void setCategoryDao(CategoryDao categoryDao) {
        SInit.categoryDao = categoryDao;
    }

    public static VariationDao getVariationDao() {
        return variationDao;
    }

    public static void setVariationDao(VariationDao variationDao) {
        SInit.variationDao = variationDao;
    }

    public static ConfigurationDao getConfigurationDao() {
        return configurationDao;
    }

    public static void setConfigurationDao(ConfigurationDao configurationDao) {
        SInit.configurationDao = configurationDao;
    }

    public void InitService(String contextLocation) throws ConfigurationException {
        try {
            System.out.println("Starting DB Init");

            Calendar timeStart = Calendar.getInstance();

            ApplicationContext context = new FileSystemXmlApplicationContext(contextLocation);
            setUserAccountDao(context.getBean("UserAccountDao", UserAccountDao.class));
            setSessionsDao(context.getBean("SessionsDao", SessionsDao.class));
            setResponseMessageDao(context.getBean("ResponseMessageDao", ResponseMessageDao.class));
            setProductDao(context.getBean("ProductDao", ProductDao.class));
            setCategoryDao(context.getBean("CategoryDao", CategoryDao.class));
            setVariationDao(context.getBean("VariationDao", VariationDao.class));
            setConfigurationDao(context.getBean("ConfigurationDao", ConfigurationDao.class));

            Calendar timeEnd = Calendar.getInstance();
            long diff = timeEnd.getTimeInMillis() - timeStart.getTimeInMillis();
            double seconds = (double) diff / 1000;
            long minutes = (long) seconds / 60000;

            System.out.println("DB Started");
            System.out.println("Init Performance: " + minutes + ":" + seconds + " seconds");

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
